import UIKit
import CoreData

class ViewController: UIViewController {
    @IBOutlet weak var tasksTableView: UITableView!
    
    var tasks: [Task] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest: NSFetchRequest<Task> = Task.fetchRequest()

        do {
            tasks = try context.fetch(fetchRequest)
        } catch let  error as NSError {
            print(error)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    @IBAction func taskAddButton(_ sender: Any) {
        let alertController = UIAlertController(title: "new task", message: "enter task, please", preferredStyle: .alert)
        let saveTask = UIAlertAction(title: "Save", style: .default) { action in
            let tf = alertController.textFields?.first
            if let newTask = tf?.text{
                self.saveTask(withTitle: newTask)
                self.tasksTableView.reloadData()
            }
        }
        
        alertController.addTextField { _ in}
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) {_ in}
        alertController.addAction(saveTask)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
  
    func saveTask(withTitle title: String){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        guard let entiy = NSEntityDescription.entity(forEntityName: "Task", in: context) else {return}
        
        let taskObject = Task(entity: entiy, insertInto: context)
        taskObject.taskName = title as! String
        
        do {
            try context.save()
            tasks.append(taskObject)
        } catch let error as NSError{
            print(error)
        }
    }
    
}


extension ViewController: UITableViewDataSource{
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! taskTableViewCell
      let task = tasks[indexPath.row]
        cell.taskLabel.text = task.taskName
      return cell
    }
  
    
    
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
     if editingStyle == .delete{
  
 //       let fetchRequest: NSFetchRequest<Task> = Task.fetchRequest()
   //     if let arrayTasks = try? context.fetch(fetchRequest){
 //           for task in arrayTasks{
     //          if task == self.tasks[indexPath.row]{
                    
        
        context.delete(self.tasks[indexPath.row])
        tasks.remove(at: indexPath.row)
        
   //         }
        }


   
    
        tableView.deleteRows(at: [indexPath], with: .top)
    
        do {
            try context.save()
        } catch let error as NSError{
            print(error)
        }
    
    }
    
  
 
    
    
}
