//
//  taskTableViewCell.swift
//  14.coreData
//
//  Created by iMac on 14/08/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class taskTableViewCell: UITableViewCell {

    @IBOutlet weak var taskLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
